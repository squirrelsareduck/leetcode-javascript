const multiplier = x < 0 ? -1 : 1;
const absX = Math.abs(x);
const reverseAbsX = absX.toString().split('').reverse().join('');
const reverseNumber = new Number(reverseAbsX) * multiplier;
if (reverseNumber < -1* Math.pow(2, 31) || reverseNumber > Math.pow(2, 31) - 1) return 0;
return reverseNumber;
