/**
 * Definition for singly-linked list.
 * function ListNode(val) {
 *     this.val = val;
 *     this.next = null;
 * }
 */
/**
 * @param {ListNode} l1
 * @param {ListNode} l2
 * @return {ListNode}
 */
var addTwoNumbers = function(l1, l2) {
    let l1Iter = l1;
    let l2Iter = l2;
    let result;
    let resultIter;
    let carryover = 0;

    while (true) {
        let val = l1Iter.val + l2Iter.val + carryover;
        carryover = 0;
        if (val > 9) {
            carryover = 1;
            val -= 10;
        }
        if (!result) {
            result = new ListNode(val);
            resultIter = result;
        } else {
            resultIter.next = new ListNode(val);
            resultIter = resultIter.next;
        }
        if (!l1Iter.next && !l2Iter.next) break;
        l1Iter = l1Iter.next ? l1Iter.next : new ListNode(0);
        l2Iter = l2Iter.next ? l2Iter.next : new ListNode(0);
    }

    if (carryover) resultIter.next = new ListNode(carryover);

    return result;
};
