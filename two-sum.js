/**
 * @param {number[]} nums
 * @param {number} target
 * @return {number[]}
 */
var twoSum = function(nums, target) {
    const map = new Map();
    for (let a=0; a<nums.length; a++) {
        map.set(target - nums[a], a);
    }

    for (let a=0; a<nums.length; a++) {
        const mapHasNumsA = map.has(nums[a]);
        if(mapHasNumsA) {
            const mapGetNumsA = map.get(nums[a]);
            if (a !== mapGetNumsA) return [a, mapGetNumsA];
        }
    }
};
