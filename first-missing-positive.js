var firstMissingPositive = function(nums) {
    const posNumbers = nums.filter(num => num > 0);

    let containsSet = new Set();
    let max = 0;
    for (const posNumber of posNumbers) {
        containsSet.add(posNumber);
        if (posNumber > max) max = posNumber;
    }

    let a = 1;
    do {
        if (!containsSet.has(a)) return a;
        a++;
    } while (a <= max);

    return a;
};
